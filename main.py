import pygame
import math
import time
from config import *  # impprting conifg.py file

pygame.init()
time1_intial = 0
time2_intial = 0
time_final2 = 0
time_final1 = 0
total1 = 0
total2 = 0
total_time1 = 0
total_time2 = 0
fishes1 = 0
fishes2 = 0
level1 = 1
level2 = 1
flag1 = 0
flag2 = 0
screen = pygame.display.set_mode((800, 655))  # loading images required in project line 22 - 30
pygame.display.set_caption("turtle")
icon = pygame.image.load("1.jpg")
pygame.display.set_icon(icon)
player1_img = pygame.image.load("3.png")
wave_img = pygame.image.load("5.png")
player2_img = pygame.image.load("2.png")
ship_img = pygame.image.load("4.png")
mount_img = pygame.image.load("beach.png")
x_1cord = 780.0  # difining different initials cordinates of different objects displayed in game screen
y_1cord = 600.0  # line 32-76
x_2cord = 0.0
y_2cord = 0.0
x1_wave = 35.0
y1_wave = 90.0
x2_wave = 400.0
x3_wave = 200.0
x4_wave = 600.0
x5_wave = 100.0
x6_wave = 500.0
x7_wave = 300.0
x8_wave = 700.0
x9_wave = 500.0
x10_wave = 800.0
y2_wave = 60.0
flag1 = 0
y3_wave = 210.0
y4_wave = 210.0
y5_wave = 365.0
y6_wave = 365.0
y7_wave = 440.0
y8_wave = 440.0
y9_wave = 570.0
y10_wave = 570.0
x1_ship = 0
y1_ship = 175
x2_ship = 150
y2_ship = 290
x4_ship = 530
y4_ship = 370
x5_ship = 40
y5_ship = 530
x11_ship = 120
y11_ship = 55
flag = 0
x6_ship = 776
y6_ship = 410
x8_ship = 434
y8_ship = 530
x9_ship = 450
y9_ship = 55
time_check1 = 0
time_check2 = 0
score1 = 0
score2 = 0
player1_chance = 1
player2_chance = 0


def waves(x_wave, y_wave):  # defining the cordinates of fishes in the display andmoving them
    global x_1cord, y_1cord, player1_chance, player2_chance, score1, score2, fishes1, fishes2, x_2cord, y_2cord
    screen.blit(wave_img, (x_wave, y_wave))
    x_wave += 0.15
    # speed of each fish
    # if anyplayer eats fish then increasing its points
    if player1_chance:
        distance1 = math.sqrt(math.pow(x_wave - (x_1cord + 32), 2) + math.pow(y_wave - (32 + y_1cord), 2))
        if distance1 < 30:
            fishes1 += 2
            x_wave -= 400
    elif player2_chance:
        distance2 = math.sqrt(math.pow(x_wave - (x_2cord + 32), 2) + math.pow(y_wave - (32 + y_2cord), 2))
        if distance2 < 30:
            fishes2 += 2
            x_wave -= 400
    if x_wave > 800:
        x_wave = -8
    return x_wave, y_wave


# reseting initial variables to their intial posiotion so for making same situatio for next player
def reset():
    global score1, score2, x_1cord, y_1cord, x_2cord, y_2cord, x5_ship, x9_ship, x_c, y_c
    x_1cord = 780.0
    y_1cord = 724.0
    x_2cord = 5.0
    y_2cord = 0.0
    x5_ship = 0
    x9_ship = 400
    x_c = 0
    y_c = 0
    score1 = 0
    score2 = 0
    p = 1
    # stopping code so the next player could set
    while p:
        screen.fill((255, 255, 255))
        score("press 'n' to play next turn", 200, 250, 40)
        pygame.display.update()
        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_n:
                    p = 0


# defining speed and movement of ships ship() for movement to right and ship1 to movement to left
def ship(x_ship, y_ship, x_1cord, y_1cord, x_2cord, y_2cord, player1_chance, player2_chance, flag):
    global flag2, running, flag1, x5_ship, x9_ship, level1, level2, total1, total2, total_time2, total_time1, time1_intial, time2_intial, time_check1, time_check2
    screen.blit(ship_img, (x_ship, y_ship))
    if player1_chance:
        x_ship += (0.18 + (2 * level1) / 10)
    if player2_chance:
        x_ship += (0.18 + (2 * level2) / 10)
    distance1 = math.sqrt(math.pow(x_ship - x_1cord, 2) + math.pow(y_ship - y_1cord, 2))
    distance2 = math.sqrt(math.pow(x_ship - x_2cord, 2) + math.pow(y_ship - y_2cord, 2))
    flag = 0
    # checking if any player collide with ship 
    # if first player colllides then last turn would be of 2nd player  to end that round and game end
    # if 2nd player dies then game over as that round is ended
    if player1_chance:
        if distance1 < 50:
            flag1 = 1
            score("Player1 crashed", 400, 250, 32)
            # reseting time for next player and adding time of first player
            total1 += score1
            total_time1 += (time_final1 - time1_intial) / 1000
            time_check2 = 0
            player1_chance = 0
            player2_chance = 1
            reset()
    elif player2_chance:
        if distance2 < 50:
            score("Player2 crashed", 300, 250, 30)
            score("GAME OVER!", 300, 450, 40)
            pygame.display.update()
            total_time2 += (time_final2 - time2_intial) / 1000
            time_check1 = 0
            time.sleep(1)
            total2 += score2
            time.sleep(0.5)
            running = False

    if x_ship > 800:
        x_ship = -32
    return x_1cord, y_1cord, x_2cord, y_2cord, x_ship, y_ship, player1_chance, player2_chance, flag


# same situation as ship() movement is towards left of ships
def ship1(x_ship, y_ship, x_1cord, y_1cord, x_2cord, y_2cord, player1_chance, player2_chance, flag):
    global flag2, running, flag1, x5_ship, x9_ship, level1, level2, total1, total2, total_time2, total_time1, time1_intial, time2_intial, time_check1, time_check2
    screen.blit(ship_img, (x_ship, y_ship))
    if player1_chance:
        x_ship -= (0.18 + (2 * level1) / 10)
    if player2_chance:
        x_ship -= (0.18 + (2 * level2) / 10)
    distance1 = math.sqrt(math.pow(x_ship - x_1cord, 2) + math.pow(y_ship - y_1cord, 2))
    distance2 = math.sqrt(math.pow(x_ship - x_2cord, 2) + math.pow(y_ship - y_2cord, 2))
    flag = 0
    if player1_chance:

        if distance1 < 50:
            flag1 = 1
            score("Player1 crashed", 300, 250, 32)
            total1 += score1
            total_time1 += (time_final1 - time1_intial) / 1000
            time_check2 = 0
            player1_chance = 0
            player2_chance = 1
            reset()

    elif player2_chance:
        if distance2 < 50:
            score("Player2 crashed", 300, 250, 30)
            score("GAME OVER!", 300, 450, 40)
            pygame.display.update()
            total_time2 += (time_final2 - time2_intial) / 1000
            time_check1 = 0
            time.sleep(1)
            total2 += score2
            time.sleep(0.5)
            running = False

    if x_ship < 0:
        x_ship = 800
    return x_1cord, y_1cord, x_2cord, y_2cord, x_ship, y_ship, player1_chance, player2_chance, flag


# checkiing and updating for points as any ship crosses any road or river
def check1():
    global score1, y_1cord, y_2cord, score2, player1_chance, player2_chance
    if player2_chance:
        if y_2cord > 600:
            score2 = 75

        elif y_2cord > 540:
            score2 = 65
        elif y_2cord > 480:
            score2 = 60
        elif y_2cord > 420:
            score2 = 50

        elif y_2cord > 360:
            score2 = 45
        elif y_2cord > 295:
            score2 = 35

        elif y_2cord > 230:
            score2 = 30
        elif y_2cord > 175:
            score2 = 20

        elif y_2cord > 115:
            score2 = 15
        elif y_2cord > 55:
            score2 = 5
    y_11cord = y_1cord + 64
    if player1_chance:
        if y_11cord <= 64:
            score1 = 75
        elif y_11cord <= 119:
            score1 = 65
        elif y_11cord <= 186:
            score1 = 60
        elif y_11cord <= 242:
            score1 = 50

        elif y_11cord <= 307:
            score1 = 45
        elif y_11cord <= 361:
            score1 = 35

        elif y_11cord <= 428:
            score1 = 30
        elif y_11cord <= 480:
            score1 = 20

        elif y_11cord <= 543:
            score1 = 15
        elif y_11cord <= 600:
            score1 = 5


# function is defined to write something on screen
def score(scoref, x, y, z):
    textfont = pygame.font.Font(font, z)
    message = textfont.render(str(scoref), True, black)
    screen.blit(message, (x, y))


# checking and updatiing movement of player1 if any event encountered
def player1(x_cord, y_cord):
    screen.blit(player1_img, (x_cord, y_cord))
    x_cord += x_c
    y_cord += y_c
    check1()
    if x_cord > 736:
        x_cord = 736
    elif x_cord < -9:
        x_cord = -9
    if y_cord > 600:
        y_cord = 600
    elif y_cord < 0:
        y_cord = 0
    return x_cord, y_cord


# cheking and updating position of player2 if any event encountered
def player2(x_cord, y_cord):
    screen.blit(player2_img, (x_cord, y_cord))
    x_cord += x_c
    y_cord += y_c
    check1()
    if x_cord > 736:
        x_cord = 736
    elif x_cord < -9:
        x_cord = -9
    if y_cord > 601:
        y_cord = 601
    elif y_cord < -2:
        y_cord = -2
    return x_cord, y_cord


# checking if any player completes level if completes then increasing its level
def level_check():
    global running, y_1cord, y_2cord, level1, level2, player1_chance, player2_chance, total1, total2, total_time2, total_time1, time1_intial, time2_intial, time_check1, time_check2
    # checking for player 1
    if player1_chance:
        if y_1cord == 0:
            player2_chance = 1
            player1_chance = 0
            level1 += 1
            total1 += score1
            total_time1 += (time_final1 - time1_intial) / 1000
            time_check2 = 0
            score("level completed", 200, 300, 40)
            pygame.display.update()
            time.sleep(0.5)
            reset()
    # checking for player 2

    elif player2_chance:
        if y_2cord > 600:
            player2_chance = 0
            player1_chance = 1
            level2 += 1
            total2 += (score2)
            total_time2 += (time_final2 - time2_intial) / 1000
            time_check1 = 0
            score("level completed", 200, 300, 40)
            pygame.display.update()
            time.sleep(0.5)
            if flag1:
                # checking for player 1
                running = 0
            else:
                reset()


# setting green stripes as roads as shown on screen
def hills(y):
    screen.fill(green, rect=[0, y, 800, 55])


# displaying mountains and killing player if it touches any mountain
def mountain(x_mount, y_mount):
    screen.blit(mount_img, (x_mount, y_mount))  # displaying mountain
    global total_time2, total_time1, time1_intial, time2_intial, time_check1, time_check2, flag2, total1, total2, running, flag1, x_1cord, y_1cord, x_2cord, y_2cord, x5_ship, x9_ship, flag, player1_chance, player2_chance
    distance1 = math.sqrt(math.pow(x_mount - x_1cord, 2) + math.pow(y_mount - y_1cord, 2))
    distance2 = math.sqrt(math.pow(x_mount - x_2cord, 2) + math.pow(y_mount - y_2cord, 2))
    flag = 0
    # checking if player 1 touches
    if player1_chance:

        if distance1 < 40:
            flag1 = 1
            total1 += score1
            score("Player1 crashed", 400, 250, 32)
            score("Player2 Turn", 400, 350, 32)
            pygame.display.update()
            time.sleep(0.5)
            total_time1 += (time_final1 - time1_intial) / 1000
            time_check2 = 0
            reset()
            player1_chance = 0
            player2_chance = 1
    # checking if player 2 touches

    elif player2_chance:
        if distance2 < 40:
            score("Player2 crashed", 300, 250, 32)
            score("GAME OVER:-)", 300, 450, 40)
            pygame.display.update()
            total_time2 += (time_final2 - time2_intial) / 1000
            time_check1 = 0
            time.sleep(1)
            total2 += (score2)
            running = 0  # stoping while loop as round ends and player dies


# display instruction untill players press defined keys to continue
running1 = 1
while running1:
    screen.fill((white))
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_c:
                running1 = 0
    score("WELCOME! to TURTLE GAME", 50, 10, 40)
    score("Instruction", 100, 60, 35)
    score("1. Cross river to get 10 points.", 20, 90, 30)
    score("2. Cross road to get 5  points.", 20, 150, 30)
    score("3. Eat fish get 2 points.", 20, 190, 30)
    score("4. Two players game.", 20, 230, 30)
    score("5. score for current round would be written", 20, 270, 30)
    score("  on the left-up corner of the screen", 20, 310, 30)
    score("6. Every round shows the points,time,level", 20, 350, 30)
    score("  of the player playing at current time respectively", 20, 390, 30)
    score("  starting from the top", 20, 430, 30)
    score("7. Every mountain work as a obstacle", 20, 470, 30)
    score("8. Winner is calculated as more points taken per second", 20, 510, 29)
    score("Press 'c' to start", 100, 600, 30)
    pygame.display.update()
    # running loop for game to have changes
running = True
x_c = 0
y_c = 0
while running:
    screen.fill((sky_blue))  # setting bsckground colour
    for event in pygame.event.get():
        if event.type == pygame.QUIT:  # quitiing if quit is definied
            running = False
        if player1_chance:

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    x_c = -1
                if event.key == pygame.K_RIGHT:
                    x_c = 1
                if event.key == pygame.K_UP:
                    y_c = -1
                if event.key == pygame.K_DOWN:
                    y_c = 1
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                    x_c = 0.0
                if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    y_c = 0.0
        if player2_chance:

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_a:
                    x_c = -1
                if event.key == pygame.K_d:
                    x_c = 1
                if event.key == pygame.K_w:
                    y_c = -1
                if event.key == pygame.K_s:
                    y_c = 1
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_a or event.key == pygame.K_d:
                    x_c = 0.0
                if event.key == pygame.K_w or event.key == pygame.K_s:
                    y_c = 0.0

    x1_wave, y1_wave = waves(x1_wave, y1_wave)  # function calling to show fishes
    x2_wave, y_2wave = waves(x2_wave, y2_wave)
    x3_wave, y_3wave = waves(x3_wave, y3_wave)
    x4_wave, y_4wave = waves(x4_wave, y4_wave)
    x5_wave, y_5wave = waves(x5_wave, y5_wave)
    x6_wave, y_6wave = waves(x6_wave, y6_wave)
    x7_wave, y_7wave = waves(x7_wave, y7_wave)
    x8_wave, y_8wave = waves(x8_wave, y8_wave)
    x9_wave, y_9wave = waves(x9_wave, y9_wave)
    x10_wave, y_10wave = waves(x10_wave, y10_wave)
    hills(0)  # function calling to show roads
    hills(120)
    hills(240)
    hills(360)
    hills(480)
    hills(600)
    hills(660)
    screen.fill((0, 0, 0), rect=[0, 55, 800, 2])  # black stripes at starting and ending line
    screen.fill((0, 0, 0), rect=[0, 600, 800, 2])

    mountain(200, 360)  # functiong calling to display mountains
    mountain(60, 120)
    mountain(500, 120)
    mountain(560, 120)
    mountain(360, 240)
    mountain(680, 360)
    mountain(60, 600)
    mountain(300, 600)
    mountain(400, 1)
    # function calling to display  ships both for right and left movement
    x_1cord, y_1cord, x_2cord, y_2cord, x1_ship, y1_ship, player1_chance, player2_chance, flag = ship1(x1_ship, y1_ship,
                                                                                                       x_1cord, y_1cord,
                                                                                                       x_2cord, y_2cord,
                                                                                                       player1_chance,
                                                                                                       player2_chance,
                                                                                                       flag)
    x_1cord, y_1cord, x_2cord, y_2cord, x2_ship, y2_ship, player1_chance, player2_chance, flag = ship(x2_ship, y2_ship,
                                                                                                      x_1cord, y_1cord,
                                                                                                      x_2cord, y_2cord,
                                                                                                      player1_chance,
                                                                                                      player2_chance,
                                                                                                      flag)
    x_1cord, y_1cord, x_2cord, y_2cord, x5_ship, y5_ship, player1_chance, player2_chance, flag = ship(x5_ship, y5_ship,
                                                                                                      x_1cord, y_1cord,
                                                                                                      x_2cord, y_2cord,
                                                                                                      player1_chance,
                                                                                                      player2_chance,
                                                                                                      flag)
    x_1cord, y_1cord, x_2cord, y_2cord, x6_ship, y6_ship, player1_chance, player2_chance, flag = ship1(x6_ship, y6_ship,
                                                                                                       x_1cord, y_1cord,
                                                                                                       x_2cord, y_2cord,
                                                                                                       player1_chance,
                                                                                                       player2_chance,
                                                                                                       flag)
    x_1cord, y_1cord, x_2cord, y_2cord, x8_ship, y8_ship, player1_chance, player2_chance, flag = ship1(x8_ship, y8_ship,
                                                                                                       x_1cord, y_1cord,
                                                                                                       x_2cord, y_2cord,
                                                                                                       player1_chance,
                                                                                                       player2_chance,
                                                                                                       flag)
    x_1cord, y_1cord, x_2cord, y_2cord, x9_ship, y9_ship, player1_chance, player2_chance, flag = ship(x9_ship, y9_ship,
                                                                                                      x_1cord, y_1cord,
                                                                                                      x_2cord, y_2cord,
                                                                                                      player1_chance,
                                                                                                      player2_chance,
                                                                                                      flag)
    x_1cord, y_1cord, x_2cord, y_2cord, x11_ship, y11_ship, player1_chance, player2_chance, flag = ship1(x11_ship,
                                                                                                         y11_ship,
                                                                                                         x_1cord,
                                                                                                         y_1cord,
                                                                                                         x_2cord,
                                                                                                         y_2cord,
                                                                                                         player1_chance,
                                                                                                         player2_chance,
                                                                                                         flag)
    # calling level check function to check whether level is completed or not
    level_check()
    if player1_chance and time_check1 < 1:
        time1_intial = pygame.time.get_ticks()
        time_check1 += 1
    elif player2_chance and time_check2 < 1:
        time2_intial = pygame.time.get_ticks()
        time_check2 += 1
    # enabling game for player 1 if its his chance
    if player1_chance:
        time_final1 = pygame.time.get_ticks()
        x_1cord, y_1cord = player1(x_1cord, y_1cord)
        score(fishes1 + score1, 750, 0, 32)
        score((time_final1 - time1_intial) / 1000, 740, 40, 30)
        score(level1, 740, 80, 32)
    # enabling game for player 2 if its his chance
    elif player2_chance:
        time_final2 = pygame.time.get_ticks()
        x_2cord, y_2cord = player2(x_2cord, y_2cord)
        score(fishes2 + score2, 750, 0, 30)
        score((time_final2 - time2_intial) / 1000, 750, 40, 30)
        score(level2, 750, 80, 30)
    pygame.display.update()
r = 1
# dispalying the final result untill the player press key to exit
while r:
    screen.fill((255, 255, 255))
    score("scoreboard", 400, 50, 40)
    score("Points", 350, 100, 32)
    score("Level", 550, 100, 32)
    score("Time", 640, 100, 32)
    o = str(total_time1)
    score("player1 total score", 10, 200, 32)
    score(total1 + fishes1, 355, 200, 32)
    score(level1, 555, 200, 32)
    score(o[0:6], 645, 200, 32)
    i = str(total_time2)

    score("player2 total score", 10, 250, 32)
    score(total2 + fishes2, 355, 250, 32)
    score(level2, 555, 250, 32)
    score(i[0:6], 645, 250, 32)
    if total_time2:
        ratio2 = (total2 + fishes2) / total_time2
    if total_time1:
        ratio1 = (total1 + fishes1) / total_time1
    if ratio1 and ratio2:
        if ratio1 > ratio2:
            score("!!player1 wins!!", 400, 500, 35)
        else:
            score("!!player2 wins!!", 400, 500, 35)
    elif ratio1 == 0:
        score("!!player2 wins!!", 400, 500, 35)
    elif ratio2 == 0:
        score("!!player1 wins!!", 400, 500, 35)

    else:
        score("Game TIE!!", 400, 500, 35)
    score("press 'e' to exit", 300, 600, 20)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_e:
                r = 0
    pygame.display.update()
