# Game_development Using Pygame library in Python  

Python game project assignment3
ISS-Assignment3:

Project name: Turtle

Project assigned during assignment3 in iiit hyderbad

Aim : To develop a  game having some roads and rivers with some obstacles

![Turtle](./Turtle.png)

## Library used 
**Pygame**


## Files 
main.py
config.py

## Rules 

Points awarded for:
1) +5 for crossing a road.
2) +10 for crossing a river.
3) +2 for eating a fish .

level completed:
If a person reaches the last road opposite to it on screen

player dies:
1) Hits a mountain or ship

Game over:
If anyone of the player dies :
1) If a player1 dies than to complete the round ,player2 chance is the last (next one).
2) If a player2 dies than game over as round completes.

## How to run the game
1. Run following command in terminal to install requirements (only once)
`pip3 install pygame`
2. Run `python3 main.py` to run the program
